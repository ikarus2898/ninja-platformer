﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.SqliteClient;
using System.Data;
using System.IO;
public class Consulta_2 : MonoBehaviour
{
    // Start is called before the first frame update
    public void consulta__()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/NinjaController/Scripts/SQL/habilidades.db");
        conexion.Open();

        IDbCommand statement = conexion.CreateCommand();
        //Borramos los datos de las tablas
        statement.CommandText = "SELECT nombreHab, posicion FROM Posiciones WHERE posicion LIKE '(-%' union SELECT nombreHab, posicion FROM Posiciones WHERE posicion LIKE '%, 2%'";
        statement.ExecuteNonQuery();
        IDataReader datos = statement.ExecuteReader();

        Debug.Log("Posiciones cuyo eje x empieza por - y cuyo eje y empieza por 2");

        while (datos.Read())
        {
            string habNombre = datos.GetString(0);
            string posicionCaja = datos.GetString(1);
            Debug.Log( habNombre + " " + posicionCaja);

        }
        conexion.Close();
    }
}