﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.SqliteClient;
using System.Data;
using System.IO;
public class Consulta_1 : MonoBehaviour
{
    // Start is called before the first frame update
    public void consulta_()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/NinjaController/Scripts/SQL/habilidades.db");
        conexion.Open();

        IDbCommand statement = conexion.CreateCommand();
        //Borramos los datos de las tablas
        statement.CommandText = "SELECT Habilidades.nombre, Posiciones.posicion FROM Habilidades INNER JOIN Posiciones ON Posiciones.NombreHab = Habilidades.nombre";
        statement.ExecuteNonQuery();
        IDataReader datos = statement.ExecuteReader();

        while (datos.Read())
        {
            string habNombre = datos.GetString(0);
            string posicionCaja = datos.GetString(1);
            Debug.Log("Habilidad Recogida: "+habNombre+" en Posición: "+posicionCaja+"");

        }
        conexion.Close();
    }
}
