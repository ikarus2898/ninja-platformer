﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.SqliteClient;
using System.Data;
using System.IO;

public class Reset : MonoBehaviour
{
    public Vector3 posInicial = new Vector3 (-20f, 20f, 0f);
    public GameObject playerAvatar;
    public void reset()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/NinjaController/Scripts/SQL/habilidades.db");
        conexion.Open();

        IDbCommand statement = conexion.CreateCommand();
        //Borramos los datos de las tablas
        statement.CommandText = "DELETE FROM Posiciones";
        statement.ExecuteNonQuery();
        statement.CommandText = "DELETE FROM Habilidades";
        statement.ExecuteNonQuery();

        conexion.Close();

        playerAvatar.GetComponent<Transform>().position = posInicial;
    }

}

