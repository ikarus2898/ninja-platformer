﻿using System.IO;
using UnityEngine;

public class savePos : MonoBehaviour


{
    string path = @"D:\Platformer 2D\2D Serialized\Assets\Serialización\posicion.json";

    void Start()
    {
        //Deserializar
        PlayerSettings player = JsonUtility.FromJson<PlayerSettings>(File.ReadAllText(path));
        transform.position = player.position;
    }
    private void Update()
    {
        //Serializar
        PlayerSettings playerSave = new PlayerSettings()
        {
            position = transform.position
        };

        File.WriteAllText(path, JsonUtility.ToJson(playerSave));
    }
}

class PlayerSettings
{
    public Vector3 position;
}
