﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.SqliteClient;
using System.Data;

public class habGravity : MonoBehaviour {
    public void Start()
    {
        string posStart = gameObject.GetComponent<Transform>().position.ToString();
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/NinjaController/Scripts/SQL/habilidades.db");
        conexion.Open();
        IDbCommand statement = conexion.CreateCommand();
        //Comprueba si el dato está ya introducido en la tabla
        statement.CommandText = "SELECT Posicion FROM Posiciones WHERE Posicion = '" + posStart + "'";
        IDataReader datos = statement.ExecuteReader();

        while (datos.Read())
        {
            string posTabla = datos.GetString(0);
            //Si el dato está en la tabla, destruye el objeto. Si no, no hace nada
            if (posTabla == posStart)
            {
                Destroy(gameObject);
            }
        }
        conexion.Close();

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/NinjaController/Scripts/SQL/habilidades.db");
        conexion.Open();

        IDbCommand statement = conexion.CreateCommand();
        statement.CommandText = "REPLACE INTO Habilidades (id, nombre) VALUES (3, 'Gravity')";
        statement.ExecuteNonQuery();

        //Introducimos la posición como id de la segunda tabla
        string pos = gameObject.GetComponent<Transform>().position.ToString();
        //Debug.Log(pos);
        IDbCommand statement3 = conexion.CreateCommand();
        //Introducimos la posición en la tabal de la BDD, que hará las veces de ID para separar esta caja concreta de otras cajas
        statement.CommandText = "INSERT INTO Posiciones (posicion, nombreHab) VALUES ('" + pos + "', 'Gravity')";
        statement.ExecuteNonQuery();

        conexion.Close();

        Destroy(gameObject);
    }
}

